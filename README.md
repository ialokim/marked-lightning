# marked-lightning

An add-on for [Mozilla Thunderbird](https://www.thunderbird.net) which enables the use of Markdown in task descriptions and renders them as HTML in the TaskView.

Header, enumerations, text styling as well as links to webpages, mail adresses and even entire mails (requires [Thunderlink](https://addons.thunderbird.net/de/thunderbird/addon/thunderlink/)) are supported.

## Notes for developers

This add-on still uses the legacy bootstrap API using the [BootstrapLoader](https://github.com/thundernest/addon-developer-support/wiki/BootstrapLoader-API:-About-bootstrap.js).
It will be eventually necessary to update the code to [the new MailExtension APIs](https://thunderbird-webextensions.readthedocs.io/en/78/)
once UI manipulation and Lightning Tasks APIs exist (see https://thunderbird.topicbox.com/groups/addons/Ta66e29a70cfa405f and https://github.com/thundernest/tb-web-ext-experiments/tree/master/calendar for some first drafts).

For some reason, TB behaves differently when loading an add-on temporarily or from an xpi file.
Be sure to double check the addon is working using the xpi file before publishing!
Also TB seems to cache old versions of files (at least of the `bootstrap.js` file) even after restart.
Therefore we need to change the name of the file before each release.
The only other workaround I could find is to start TB with the `-purgecaches` option.

## Credits

The source code is loosely inspired by the outdated [AdvancedTasks](https://addons.thunderbird.net/de/thunderbird/addon/advancedtasks/) add-on and adapted to be usable in Thunderbird 68+.
It uses [marked](https://github.com/markedjs/marked) under the hood to render markdown content.

## Contributing

Feel free to report any problems or suggest features in the [issue tracker](https://gitlab.com/ialokim/marked-lightning/issues)
or collaborate on making this add-on future-proof by converting the legacy bootstrap code to the new MailExtension or WebExtensions Experiments API (see https://gitlab.com/ialokim/marked-lightning/issues/1).

## Licence

The addon is licenced under the terms of [Mozilla Public Licence 2.0](LICENCE.md).
